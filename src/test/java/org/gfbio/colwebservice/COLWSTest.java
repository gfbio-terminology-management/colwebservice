package org.gfbio.colwebservice;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import javax.json.JsonObject;
import org.junit.Test;

public class COLWSTest {

  @Test
  public void testGetHierarchy() {
    CatalogueOfLife c = new CatalogueOfLife();
    JsonObject jo = c.getHierarchy(
        "http://api.cybertaxonomy.org/col/name_catalogue/taxon.json?taxonUuid=a031b7b2-0803-4cd1-958e-d8e7376dbe2d")
        .create().get(0);
    assertNotNull(jo);
  }

  @Test
  public void testGetMetadata() {
    CatalogueOfLife c = new CatalogueOfLife();
    JsonObject jo = c.getMetadata().create().get(0);

    assertEquals("Catalogue Of Life", jo.get("name").toString().replace("\"", ""));
    assertEquals("COL", jo.get("acronym").toString().replace("\"", ""));
  }

  @Test
  public void testGetTermInfosCombined() {
    CatalogueOfLife c = new CatalogueOfLife();
    JsonObject jo = c.getTermInfosCombined(
        "http://api.cybertaxonomy.org/col/name_catalogue/taxon.json?taxonUuid=a031b7b2-0803-4cd1-958e-d8e7376dbe2d")
        .create().get(0);

    assertEquals("Species", jo.get("rank").toString().replace("\"", ""));
  }

  @Test
  public void testGetTermInfosOriginal() {
    CatalogueOfLife c = new CatalogueOfLife();
    JsonObject jo = c.getTermInfosOriginal(
        "http://api.cybertaxonomy.org/col/name_catalogue/taxon.json?taxonUuid=a031b7b2-0803-4cd1-958e-d8e7376dbe2d")
        .create().get(0);

    assertEquals("Abies alba Mill.", jo.get("name").toString().replace("\"", ""));
    assertEquals("Species", jo.get("rank").toString().replace("\"", ""));
  }

  @Test
  public void testGetTermInfosProcessed() {
    CatalogueOfLife c = new CatalogueOfLife();
    JsonObject jo = c.getTermInfosProcessed(
        "http://api.cybertaxonomy.org/col/name_catalogue/taxon.json?taxonUuid=a031b7b2-0803-4cd1-958e-d8e7376dbe2d")
        .create().get(0);

    assertEquals("Abies alba Mill.", jo.get("label").toString().replace("\"", ""));
    assertEquals("Species", jo.get("rank").toString().replace("\"", ""));
  }

  @Test
  public void testGetAllBroader() {
    CatalogueOfLife c = new CatalogueOfLife();
    JsonObject jo = c.getAllBroader("a031b7b2-0803-4cd1-958e-d8e7376dbe2d").create().get(0);

    assertEquals("Plantae", jo.getString("label"));
    assertEquals("Kingdom", jo.getString("rank"));
  }

  @Test
  public void testSearchExact() {
    CatalogueOfLife c = new CatalogueOfLife();
    JsonObject jo = c.search("Platalea leucorodia", "exact").create().get(0);

    assertEquals("Platalea leucorodia Linnaeus, 1758", jo.getString("label"));
    assertEquals("Animalia", jo.getString("kingdom"));

    jo = c.search("Apus", "exact").create().get(0);

    assertEquals("Apus", jo.getString("label"));
    assertEquals("Animalia", jo.getString("kingdom"));
  }

  @Test
  public void testSearchFuzzy() {
    CatalogueOfLife c = new CatalogueOfLife();
    JsonObject jo = c.search("ixodes", "fuzzy").create().get(0);

    assertEquals("Ixodes", jo.getString("label"));
    assertEquals("Animalia", jo.getString("kingdom"));

    jo = c.search("Apus", "fuzzy").create().get(0);

    assertEquals("Apus", jo.getString("label"));
    assertEquals("Animalia", jo.getString("kingdom"));
  }

}
