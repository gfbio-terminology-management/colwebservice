package org.gfbio.colwebservice;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.SocketTimeoutException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonValue;
import org.apache.log4j.Logger;
import org.gfbio.config.WSConfiguration;
import org.gfbio.interfaces.WSInterface;
import org.gfbio.resultset.AllBroaderResultSetEntry;
import org.gfbio.resultset.CapabilitiesResultSetEntry;
import org.gfbio.resultset.GFBioResultSet;
import org.gfbio.resultset.HierarchyResultSetEntry;
import org.gfbio.resultset.MetadataResultSetEntry;
import org.gfbio.resultset.SearchResultSetEntry;
import org.gfbio.resultset.SynonymsResultSetEntry;
import org.gfbio.resultset.TermCombinedResultSetEntry;
import org.gfbio.resultset.TermOriginalResultSetEntry;
import org.gfbio.util.YAMLConfigReader;

/**
 * COL Webservice implementation in the GFBio Terminology Service. This class implements all
 * necessary methods for the TS endpoints according to the WSInterface defined in the gfbioapi
 * project.
 * 
 * @author nkaram <A HREF="mailto:naouel.karam@fu-berlin.de">Naouel Karam</A>
 * 
 */

public class CatalogueOfLife implements WSInterface {

  private static final Logger LOGGER = Logger.getLogger(CatalogueOfLife.class);

  private static final String NAME_SERVICE_URL =
      "http://api.cybertaxonomy.org/col/name_catalogue.json?query=";
  private static final String TERM_SERVICE_URL =
      "http://api.cybertaxonomy.org/col/name_catalogue/taxon.json?taxonUuid=";
  private boolean isResponding = true;

  // private static final Domains[] domains = { Domains.Biology };
  // private static final Services[] availableServices = { Services.term,
  // Services.search, Services.broader, Services.allbroader, Services.hierarchy };
  // private static final SearchModes[] searchModes = { SearchModes.exact };

  // properties from YAML file
  private WSConfiguration wsConfig;

  public CatalogueOfLife() {

    YAMLConfigReader reader = YAMLConfigReader.getInstance();
    wsConfig = reader.getWSConfig("COL");

    LOGGER.info(" a COL webservice is ready to use");
  }

  public List<String> getDomains() {
    List<String> l = new ArrayList<String>();
    for (Domains d : wsConfig.getWsDomains()) {
      l.add(d.name());
    }
    return l;
  }

  public String getCurrentVersion() {
    return wsConfig.getVersion();
  }

  public String getAcronym() {
    return wsConfig.getAcronym();
  }

  public String getDescription() {
    return wsConfig.getDescription();
  }

  public String getName() {
    return wsConfig.getName();
  }

  public String getURI() {
    return wsConfig.getUri();
  }

  public boolean supportsMatchType(String match_type) {
    return Arrays.stream(wsConfig.getSearchModes())
        .anyMatch(SearchModes.valueOf(match_type)::equals);
  }

  /**
   * Implements the search method of the WSInterface Searches for names and common names in the COL
   * database based on the match type "exact" or "included"
   * 
   * @return GFBioResultSet
   */
  public GFBioResultSet<SearchResultSetEntry> search(String query, String match_type) {
    LOGGER.debug("search query " + query + " matchtype " + match_type);
    StringBuilder url = new StringBuilder();
    url.append(CatalogueOfLife.NAME_SERVICE_URL);
    GFBioResultSet<SearchResultSetEntry> rs = new GFBioResultSet<SearchResultSetEntry>("col");
    if (match_type.equals(SearchTypes.included.name())) {
      rs.addWarning("COL does not support matchtype \"" + SearchTypes.included.name() + "\". "
          + "Displaying exact results instead.");
    }
    try {
      query = URLEncoder.encode(query, "UTF-8");
      url.append(query);
      for (String uuid : getUuidsFromSearchResult(HTTPRequest.get(url.toString()))) {
        StringBuilder term_url = new StringBuilder();
        term_url.append(CatalogueOfLife.TERM_SERVICE_URL);
        uuid = URLEncoder.encode(uuid, "UTF-8");
        term_url.append(uuid);
        rs.addEntry(createSearchResult(HTTPRequest.get(term_url.toString())));
      }
      LOGGER.info("search query processed " + query + " matchtype " + match_type);
      return rs;
    } catch (UnsupportedEncodingException e) {
      throw new GFBioBadRequestException();
    } catch (NullPointerException e) {
      return rs;
    } catch (SocketTimeoutException e) {
      isResponding = false;
    }
    return rs;
  }

  /**
   * Implements the getTermInfosOriginal method of the WSInterface Gets the informations of a taxon
   * given its URI with the original COL attributes
   * 
   * @return GFBioResultSet
   */
  public GFBioResultSet<TermOriginalResultSetEntry> getTermInfosOriginal(String term_uri) {
    GFBioResultSet<TermOriginalResultSetEntry> rs =
        new GFBioResultSet<TermOriginalResultSetEntry>("col");
    TermOriginalResultSetEntry e = new TermOriginalResultSetEntry();
    LOGGER.info("term query " + term_uri);
    StringBuilder url = new StringBuilder();
    if (!term_uri.startsWith(TERM_SERVICE_URL)) {
      url.append(CatalogueOfLife.TERM_SERVICE_URL);
    }
    String result = "";
    try {
      url.append(term_uri);
      result = HTTPRequest.get(url.toString());
    } catch (SocketTimeoutException ex) {
      isResponding = false;
    }
    result = result.substring(1, result.length() - 1);
    InputStream in = new ByteArrayInputStream(result.getBytes());
    JsonReader reader = Json.createReader(in);
    JsonObject obj = reader.readObject();
    if (obj.getJsonObject("response").containsKey("taxon")) {
      JsonObject taxon = obj.getJsonObject("response").getJsonObject("taxon");
      if (obj.getJsonObject("response").containsKey("relatedTaxa")) {
        JsonArray relatedTaxa = obj.getJsonObject("response").getJsonArray("relatedTaxa");
        if (!relatedTaxa.isEmpty()) {
          ArrayList<String> relatedTaxaList = new ArrayList<String>();
          for (int i = 0; i < relatedTaxa.size(); i++) {
            relatedTaxaList.add(((JsonObject) relatedTaxa.get(i)).getString("name"));
          }
          e.setOriginalTermInfo("relatedTaxa", relatedTaxaList);
        }
      }
      if (taxon.containsKey("name")) {
        e.setOriginalTermInfo("name", taxon.getString("name"));
      }
      if (taxon.containsKey("rank")) {
        e.setOriginalTermInfo("rank", taxon.getString("rank"));
      }
      if (taxon.containsKey("taxonStatus")) {
        e.setOriginalTermInfo("taxonStatus", taxon.getString("taxonStatus"));
      }
      if (obj.getJsonObject("response").getJsonObject("taxon").getJsonObject("classification")
          .containsKey("Kingdom")) {
        e.setOriginalTermInfo("kingdom", obj.getJsonObject("response").getJsonObject("taxon")
            .getJsonObject("classification").getString("Kingdom"));
      }
      if (obj.getJsonObject("request").containsKey("taxonUuid")) {
        e.setOriginalTermInfo("taxonUuid", obj.getJsonObject("request").getString("taxonUuid"));
      }
      if (taxon.containsKey("source")) {
        if (taxon.getJsonObject("source").containsKey("datasetName")) {
          e.setOriginalTermInfo("datasetname",
              taxon.getJsonObject("source").getString("datasetName"));
        }
      }
      if (taxon.containsKey("lsid")) {
        e.setOriginalTermInfo("lsid", taxon.getString("lsid"));
      }
      if (taxon.containsKey("title")) {
        e.setOriginalTermInfo("title", taxon.getString("title"));
      }
      rs.addEntry(e);
      return rs;
    }
    return null;
  }

  /**
   * Implements the getTermInfosProcessed method of the WSInterface Gets the informations of a taxon
   * given its URI with TS attributes
   * 
   * @return GFBioResultSet
   */
  public GFBioResultSet<SearchResultSetEntry> getTermInfosProcessed(String term_uri) {
    GFBioResultSet<SearchResultSetEntry> rs = new GFBioResultSet<SearchResultSetEntry>("col");
    SearchResultSetEntry e = new SearchResultSetEntry();
    LOGGER.info("term query " + term_uri);
    StringBuilder url = new StringBuilder();
    if (!term_uri.startsWith(TERM_SERVICE_URL)) {
      url.append(CatalogueOfLife.TERM_SERVICE_URL);
    }
    String result = "";
    try {
      url.append(term_uri);
      result = HTTPRequest.get(url.toString());
    } catch (SocketTimeoutException ex) {
      isResponding = false;
    }
    result = result.substring(1, result.length() - 1);
    InputStream in = new ByteArrayInputStream(result.getBytes());
    JsonReader reader = Json.createReader(in);
    JsonObject obj = reader.readObject();
    if (obj.getJsonObject("response").containsKey("taxon")) {
      JsonObject taxon = obj.getJsonObject("response").getJsonObject("taxon");

      if (taxon.containsKey("name")) {
        e.setLabel(taxon.getString("name"));
      }
      if (taxon.containsKey("rank")) {
        e.setRank(taxon.getString("rank"));
      }
      if (taxon.containsKey("taxonStatus")) {
        e.setStatus(taxon.getString("taxonStatus"));
      }
      if (obj.getJsonObject("response").getJsonObject("taxon").getJsonObject("classification")
          .containsKey("Kingdom")) {
        e.setKingdom(obj.getJsonObject("response").getJsonObject("taxon")
            .getJsonObject("classification").getString("Kingdom"));
      }
      if (obj.getJsonObject("request").containsKey("taxonUuid")) {
        e.setExternalID(obj.getJsonObject("request").getString("taxonUuid"));
        e.setUri(TERM_SERVICE_URL + obj.getJsonObject("request").getString("taxonUuid"));
      }
      if (taxon.containsKey("source")) {
        if (taxon.getJsonObject("source").containsKey("datasetName")) {
          e.setEmbeddedDatabase(taxon.getJsonObject("source").getString("datasetName"));
        }
      }
      e.setSourceTerminology(getAcronym());
      rs.addEntry(e);
      return rs;
    }
    return null;
  }

  /**
   * Implements the getTermInfosCombined method of the WSInterface Gets the informations of a taxon
   * given its URI with TS attributes when mapped and original COL attributes when not
   * 
   * @return GFBioResultSet
   */
  public GFBioResultSet<TermCombinedResultSetEntry> getTermInfosCombined(String term_uri) {
    GFBioResultSet<TermCombinedResultSetEntry> rs =
        new GFBioResultSet<TermCombinedResultSetEntry>("col");
    TermCombinedResultSetEntry e = new TermCombinedResultSetEntry();
    LOGGER.info("term query " + term_uri);
    StringBuilder url = new StringBuilder();
    if (!term_uri.startsWith(TERM_SERVICE_URL)) {
      url.append(CatalogueOfLife.TERM_SERVICE_URL);
    }
    String result = "";
    try {
      url.append(term_uri);
      result = HTTPRequest.get(url.toString());
    } catch (SocketTimeoutException ex) {
      isResponding = false;
    }
    result = result.substring(1, result.length() - 1);
    InputStream in = new ByteArrayInputStream(result.getBytes());
    JsonReader reader = Json.createReader(in);
    JsonObject obj = reader.readObject();
    if (obj.getJsonObject("response").containsKey("taxon")) {
      JsonObject taxon = obj.getJsonObject("response").getJsonObject("taxon");
      if (obj.getJsonObject("response").containsKey("relatedTaxa")) {
        JsonArray relatedTaxa = obj.getJsonObject("response").getJsonArray("relatedTaxa");
        if (!relatedTaxa.isEmpty()) {
          ArrayList<String> relatedTaxaList = new ArrayList<String>();
          for (int i = 0; i < relatedTaxa.size(); i++) {
            relatedTaxaList.add(((JsonObject) relatedTaxa.get(i)).getString("name"));
          }
          e.setOriginalTermInfo("relatedTaxa", relatedTaxaList);
        }
      }
      if (taxon.containsKey("name")) {
        e.setLabel(taxon.getString("name"));
      }
      if (taxon.containsKey("rank")) {
        e.setRank(taxon.getString("rank"));
      }
      if (taxon.containsKey("taxonStatus")) {
        e.setStatus(taxon.getString("taxonStatus"));
      }
      if (obj.getJsonObject("response").getJsonObject("taxon").getJsonObject("classification")
          .containsKey("Kingdom")) {
        e.setKingdom(obj.getJsonObject("response").getJsonObject("taxon")
            .getJsonObject("classification").getString("Kingdom"));
      }
      if (obj.getJsonObject("request").containsKey("taxonUuid")) {
        e.setExternalID(obj.getJsonObject("request").getString("taxonUuid"));
        e.setUri(TERM_SERVICE_URL + obj.getJsonObject("request").getString("taxonUuid"));
      }
      if (taxon.containsKey("source")) {
        if (taxon.getJsonObject("source").containsKey("datasetName")) {
          e.setEmbeddedDatabase(taxon.getJsonObject("source").getString("datasetName"));
        }
      }
      e.setSourceTerminology(getAcronym());
      if (taxon.containsKey("lsid")) {
        e.setOriginalTermInfo("lsid", taxon.getString("lsid"));
      }
      if (taxon.containsKey("title")) {
        e.setOriginalTermInfo("title", taxon.getString("title"));
      }
      rs.addEntry(e);
      return rs;
    }
    return null;
  }

  private SearchResultSetEntry createSearchResult(String result) {
    result = result.substring(1, result.length() - 1);
    InputStream in = new ByteArrayInputStream(result.getBytes());
    JsonReader reader = Json.createReader(in);
    JsonObject obj = reader.readObject();
    SearchResultSetEntry e = new SearchResultSetEntry();
    if (obj.containsKey("response")) {
      if (obj.getJsonObject("response").getJsonObject("taxon").getJsonObject("source")
          .containsKey("datasetName")) {
        if (obj.getJsonObject("response").getJsonObject("taxon").containsKey("name")) {
          e.setLabel(obj.getJsonObject("response").getJsonObject("taxon").getString("name"));
        }
        if (obj.getJsonObject("response").getJsonObject("taxon").containsKey("rank")) {
          e.setRank(obj.getJsonObject("response").getJsonObject("taxon").getString("rank"));
        }
        if (obj.getJsonObject("response").getJsonObject("taxon").getJsonObject("classification")
            .containsKey("Kingdom")) {
          e.setKingdom(obj.getJsonObject("response").getJsonObject("taxon")
              .getJsonObject("classification").getString("Kingdom"));
        }
        if (obj.getJsonObject("request").containsKey("taxonUuid")) {
          e.setExternalID(obj.getJsonObject("request").getString("taxonUuid"));
          e.setUri(TERM_SERVICE_URL + obj.getJsonObject("request").getString("taxonUuid"));
        }
        if (obj.getJsonObject("response").getJsonObject("taxon").containsKey("taxonStatus")) {
          e.setStatus(
              obj.getJsonObject("response").getJsonObject("taxon").getString("taxonStatus"));
        }
        e.setSourceTerminology(getAcronym());
        if (obj.getJsonObject("response").getJsonObject("taxon").getJsonObject("source")
            .containsKey("datasetName")) {
          e.setEmbeddedDatabase(obj.getJsonObject("response").getJsonObject("taxon")
              .getJsonObject("source").getString("datasetName"));
        }
      }
      e.setInternal("false");
    }
    return e;
  }

  private ArrayList<String> getUuidsFromSearchResult(String result) {
    result = result.substring(1, result.length() - 1);
    InputStream in = new ByteArrayInputStream(result.getBytes());
    JsonReader reader = Json.createReader(in);
    JsonObject obj = reader.readObject();
    ArrayList<String> uuids = new ArrayList<String>();
    if (obj.containsKey("response")) {
      for (JsonValue uuid : obj.getJsonArray("response").getJsonObject(0)
          .getJsonArray("acceptedTaxonUuids")) {
        String id = uuid.toString();
        id = id.substring(1, id.length() - 1);
        uuids.add(id);
      }
      return uuids;
    }
    return null;
  }

  public boolean isResponding() {
    try {
      HTTPRequest.get(NAME_SERVICE_URL + "platalea");
      LOGGER.info("COL is responding.");
      isResponding = true;
    } catch (SocketTimeoutException e) {
      LOGGER.info("COL is not responding.");
      isResponding = false;
    }
    return isResponding;
  }

  /**
   * Implements the getAllBroader method of the WSInterface Gets all broader terms of a taxon given
   * its COL URI or External ID (ITIS Taxonomic Serial Number)
   * 
   * @return GFBioResultSet
   */
  public GFBioResultSet<AllBroaderResultSetEntry> getAllBroader(String term_uri) {
    // LOGGER.info("allbroader query " + term_uri);
    StringBuilder url = new StringBuilder();
    if (!term_uri.startsWith(TERM_SERVICE_URL)) {
      url.append(CatalogueOfLife.TERM_SERVICE_URL);
    }
    try {
      url.append(term_uri);
      return createAllBroaderResult(HTTPRequest.get(url.toString()));
    } catch (SocketTimeoutException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    return null;
  }

  private GFBioResultSet<AllBroaderResultSetEntry> createAllBroaderResult(String result) {
    result = result.substring(1, result.length() - 1);
    InputStream in = new ByteArrayInputStream(result.getBytes());
    JsonReader reader = Json.createReader(in);
    JsonObject obj = reader.readObject();

    String name = obj.getJsonObject("response").getJsonObject("taxon").getString("name");
    GFBioResultSet<AllBroaderResultSetEntry> rs =
        new GFBioResultSet<AllBroaderResultSetEntry>("col");
    if (obj.getJsonObject("response").getJsonObject("taxon").containsKey("classification")) {
      JsonObject classification =
          obj.getJsonObject("response").getJsonObject("taxon").getJsonObject("classification");
      for (String key : classification.keySet()) {
        AllBroaderResultSetEntry e = new AllBroaderResultSetEntry();
        if (!classification.getString(key).equals(name)) {
          e.setLabel(classification.getString(key));
          e.setRank(key);
          StringBuilder url = new StringBuilder();
          url.append(CatalogueOfLife.NAME_SERVICE_URL);
          String query;
          ArrayList<String> ids = null;
          try {
            query = URLEncoder.encode(classification.getString(key), "UTF-8");
            url.append(query);
            ids = getUuidsFromSearchResult(HTTPRequest.get(url.toString()));
          } catch (UnsupportedEncodingException ex) {
            // TODO Auto-generated catch block
            ex.printStackTrace();
          } catch (SocketTimeoutException ex) {
            // TODO Auto-generated catch block
            ex.printStackTrace();
          }
          StringBuilder id_list = new StringBuilder();
          if (ids != null) {
            e.setUri(TERM_SERVICE_URL + ids.get(0));
            while (!ids.isEmpty()) {
              id_list.append(ids.get(0));
              ids.remove(0);
              if (!ids.isEmpty()) {
                id_list.append(", ");
              }
            }
          } else {
            id_list.append("not found.");
          }
          e.setExternalID(id_list.toString());

        }
        rs.addEntry(e);
      }
    }
    return rs;
  }

  /**
   * Implements the getHierarchy method of the WSInterface Gets the broader hierarchy of a taxon
   * given its COL URI or External ID (COL Taxonomic Serial Number)
   * 
   * @return GFBioResultSet
   */
  public GFBioResultSet<HierarchyResultSetEntry> getHierarchy(String term_uriORexternalID) {
    // LOGGER.info("hierarchy query " + term_uriORexternalID);
    StringBuilder url = new StringBuilder();
    if (!term_uriORexternalID.startsWith(TERM_SERVICE_URL)) {
      url.append(CatalogueOfLife.TERM_SERVICE_URL);
    }
    String result = "";
    try {
      url.append(term_uriORexternalID);
      result = HTTPRequest.get(url.toString());
    } catch (SocketTimeoutException e) {
      isResponding = false;
    }
    result = result.substring(1, result.length() - 1);
    InputStream in = new ByteArrayInputStream(result.getBytes());
    JsonReader reader = Json.createReader(in);
    JsonObject obj = reader.readObject();
    GFBioResultSet<HierarchyResultSetEntry> rs = new GFBioResultSet<HierarchyResultSetEntry>("col");
    if (obj.getJsonObject("response").getJsonObject("taxon").containsKey("classification")) {
      JsonObject classification =
          obj.getJsonObject("response").getJsonObject("taxon").getJsonObject("classification");
      Object[] keys = classification.keySet().toArray();
      for (int i = keys.length - 1; i >= 0; i--) {
        HierarchyResultSetEntry e = new HierarchyResultSetEntry();
        e.setLabel(classification.getString(keys[i].toString()));
        e.setRank(keys[i].toString());
        url = new StringBuilder();
        url.append(CatalogueOfLife.NAME_SERVICE_URL);
        String query;
        ArrayList<String> ids = new ArrayList<String>();
        ArrayList<String> parentIds = new ArrayList<String>();
        try {
          if (!classification.getString(keys[i].toString()).equals("Not assigned")) {
            query = URLEncoder.encode(classification.getString(keys[i].toString()), "UTF-8");
            url.append(query);
            ids = getUuidsFromSearchResult(HTTPRequest.get(url.toString()));
          } else {
            ids.add("Not assigned");
          }
          if (i > 0) {
            if (!classification.getString(keys[i - 1].toString()).equals("Not assigned")) {
              url = new StringBuilder();
              url.append(CatalogueOfLife.NAME_SERVICE_URL);
              query = URLEncoder.encode(classification.getString(keys[i - 1].toString()), "UTF-8");
              url.append(query);
              parentIds = getUuidsFromSearchResult(HTTPRequest.get(url.toString()));
            } else {
              parentIds.add("Not assigned");
            }
          } else {
            parentIds = null;
          }
        } catch (UnsupportedEncodingException ex) {
          // TODO Auto-generated catch block
          ex.printStackTrace();
        } catch (SocketTimeoutException ex) {
          // TODO Auto-generated catch block
          ex.printStackTrace();
        }
        StringBuilder id_list = new StringBuilder();
        StringBuilder parentId_list = new StringBuilder();
        if (ids != null) {
          e.setUri(TERM_SERVICE_URL + ids.get(0));
          while (!ids.isEmpty()) {
            id_list.append(ids.get(0));
            ids.remove(0);
            if (!ids.isEmpty()) {
              id_list.append(", ");
            }
          }
        } else {
          id_list.append("not found.");
        }
        if (parentIds != null) {
          while (!parentIds.isEmpty()) {
            if (!parentIds.get(0).equals("Not assigned")) {
              parentId_list.append(TERM_SERVICE_URL + parentIds.get(0));
            } else {
              parentId_list.append(parentIds.get(0));
            }
            parentIds.remove(0);
            if (!parentIds.isEmpty()) {
              parentId_list.append(", ");
            }
          }
        }
        if (i == keys.length - 1) {
          e.setExternalID(term_uriORexternalID);
          e.setUri(TERM_SERVICE_URL + term_uriORexternalID);
        } else {
          e.setExternalID(id_list.toString());
          if (ids != null) {
          }
        }
        ArrayList<String> array = new ArrayList<String>();
        if (parentId_list.length() > 0) {
          array.add(parentId_list.toString());
        }
        e.setHierarchy(array);
        rs.addEntry(e);
      }
      return rs;
    }
    return null;
  }

  /**
   * Implements the getSynonyms method of the WSInterface Gets the list of synonyms of a taxon given
   * its COL URI or External ID (COL Taxonomic Serial Number)
   * 
   * @return GFBioResultSet
   */
  public GFBioResultSet<SynonymsResultSetEntry> getSynonyms(String term_uriORexternalID) {
    // Synonyms not available in COL
    return null;
  }

  /**
   * Implements the getMetadata method of the WSInterface Gets the metadata information about the
   * COL webservice
   * 
   * @return GFBioResultSet
   */
  public GFBioResultSet<MetadataResultSetEntry> getMetadata() {
    GFBioResultSet<MetadataResultSetEntry> rs = new GFBioResultSet<MetadataResultSetEntry>("col");
    MetadataResultSetEntry e = new MetadataResultSetEntry();
    e.setName(getName());
    e.setAcronym(getAcronym());
    e.setContact(wsConfig.getContact());
    e.setVersion("");
    e.setDescription(getDescription());
    e.setKeywords("");
    e.setUri(getURI());
    e.setContribution("no");
    e.setStorage("external");
    ArrayList<String> list = new ArrayList<String>();
    list.add("urn:lsid:catalogueoflife.org:taxon:");
    e.setNamespaces(list);
    e.setKeywords("Species checklist, Taxonomy");
    ArrayList<String> domainUris = new ArrayList<String>();
    for (Domains d : wsConfig.getWsDomains()) {
      domainUris.add(d.getUri());
    }
    e.setDomain(domainUris);
    rs.addEntry(e);
    return rs;
  }

  /**
   * Implements the getCapabilities method of the WSInterface Returns the available service
   * endpoints for the COL webservice
   * 
   * @return GFBioResultSet
   */
  public GFBioResultSet<CapabilitiesResultSetEntry> getCapabilities() {
    GFBioResultSet<CapabilitiesResultSetEntry> rs =
        new GFBioResultSet<CapabilitiesResultSetEntry>("col");
    CapabilitiesResultSetEntry e = new CapabilitiesResultSetEntry();
    ArrayList<String> servicesArray = new ArrayList<String>();
    for (Services s : wsConfig.getAvailableServices()) {
      servicesArray.add(s.toString());
    }
    e.setAvailableServices(servicesArray);

    ArrayList<String> modesArray = new ArrayList<String>();
    for (SearchModes m : wsConfig.getSearchModes()) {
      modesArray.add(m.toString());
    }
    e.setSearchModes(modesArray);
    rs.addEntry(e);
    return rs;
  }
}
