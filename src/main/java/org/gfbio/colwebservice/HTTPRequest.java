package org.gfbio.colwebservice;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.SocketTimeoutException;
import java.net.URL;

public class HTTPRequest{
	public static String get(String url) throws SocketTimeoutException{
		  HttpURLConnection connection;
	      BufferedReader rd;
	      StringBuilder sb;
	      String line;
	      URL serverAddress;
	      
	      try {
	          serverAddress = new URL(url);		         		        
	          connection = (HttpURLConnection)serverAddress.openConnection();
	          connection.setRequestMethod("GET");
	          connection.setDoOutput(true);
	          connection.setReadTimeout(10000);
	          connection.setConnectTimeout(20000);
	                    
	          connection.connect();
	        
	          rd  = new BufferedReader(new InputStreamReader(connection.getInputStream()));
	          sb = new StringBuilder();
	          
	          while ((line = rd.readLine()) != null){
	              sb.append(line + '\n');
	          }
	          connection.disconnect();
	          return  sb.toString();
	          
	      } catch (MalformedURLException e) {
	          e.printStackTrace();
	      } catch (ProtocolException e) {
	          e.printStackTrace();
          } catch (SocketTimeoutException e){
              throw new SocketTimeoutException();
	      } catch (IOException e) {
	          e.printStackTrace();
	      }
	      return null;
	}
}
